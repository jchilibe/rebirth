﻿using UnityEngine;
using System.Collections;

public class EnemyBounce : MonoBehaviour {
	float initX;
	public float moveSpeed;

	// Use this for initialization
	void Start () {
		initX = transform.position.x;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		var vel = rigidbody2D.velocity;
		vel.y = moveSpeed;
		rigidbody2D.velocity = vel;
		Vector3 tp = transform.position;
		tp.x = initX;
		transform.position = tp;
	}

	void OnCollisionEnter2D(Collision2D coll){
		moveSpeed *= -1;
	}
}
