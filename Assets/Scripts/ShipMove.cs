﻿using UnityEngine;
using System.Collections;

public class ShipMove : MonoBehaviour {
	bool inBounds = true;
	Vector3 initPos;
	public bool levelComplete = false;


	// Use this for initialization
	void Start () {
		initPos = transform.position;
	}

	// Update is called once per frame
	void Update () {
		//if (!inBounds) resetPos();
		//else inBounds = false;
	}

	void OnMouseOver(){
		if (Input.GetMouseButtonDown(1)){
			resetPos();
		}
	}

	void OnCollisionEnter2D(){
		//cam.GetComponent<CameraControl>().screenShake = 50;
	}

	void OnTriggerExit2D(Collider2D t){
		if (t.gameObject.tag == "BG" && levelComplete == false) resetPos();
	}

	void OnTriggerEnter2D(){
		inBounds = true;
	}

	public void resetPos(){
		transform.position = initPos;
		rigidbody2D.velocity = new Vector2(0,0);
	}
	
	void FixedUpdate () {
		/*if (isGrappling && targetObj)
		{	
			Vector2 dist = -(transform.position - targetObj.transform.position);
			this.rigidbody2D.AddForce(dist*passiveRopePull);
			if (dist.magnitude > ropeLength) this.rigidbody2D.AddForce(dist.normalized*activeRopePull);
			dist.Normalize();
			float rot_z = Mathf.Atan2(dist.y, dist.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 50);
			rigidbody2D.angularVelocity = 0;
		}
		if (!overspeed && rigidbody2D.velocity.magnitude > overspeedVel){
			overspeed = true;
			aSources[2].Play ();
		}
		else if (overspeed && rigidbody2D.velocity.magnitude < overspeedVel){
			overspeed = false;
		}*/
	}
}
