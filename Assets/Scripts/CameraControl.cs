﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	public GameObject followTarget;
	public int screenShake = 0;
	public bool levelComplete=false;

	// Use this for initialization
	void Start () {
		levelComplete = false;
		Vector3 tp = transform.position;
		tp.z = -50;
		//tp.x = -1000;
		transform.position = tp;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = followTarget.transform.position;
		Vector3 dist = pos-transform.position;
		dist.z = 0;
		transform.position += dist/10;
		//if ((followTarget.GetComponent("Grappling2") as Grappling2).isGrappling)
		    transform.position += (Vector3) followTarget.rigidbody2D.velocity/100;
		//float rDist = transform.rotation.eulerAngles.z - followTarget.transform.rotation.eulerAngles.z;
		Quaternion rt = transform.rotation;
		Vector3 ea = rt.eulerAngles;
		ea.z = followTarget.rigidbody2D.velocity.x/10;
		rt.eulerAngles = ea;
		transform.rotation = rt;

		if (screenShake > 0){
			Vector3 lp = Random.insideUnitSphere*0.25f;
			lp.z = 0;
			transform.localPosition += lp;
			screenShake--;
		}

		var tp = transform.position;

		if (levelComplete){
			tp.z -= (tp.z + 80)/20;
			tp.x ++;
			if (tp.z < -79) Application.LoadLevel(Application.loadedLevel+1);
		}
		else{
			tp.z -= (tp.z + 25)/50;
		}

		transform.position = tp;
	}
}
