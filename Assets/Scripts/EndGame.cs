﻿using UnityEngine;
using System.Collections;

public class EndGame : MonoBehaviour {
	
	int sNum=0;
	public Sprite pic1,pic2,pic3,pic4,pic5;
	ArrayList al;
	
	// Use this for initialization
	void Start () {
		al = new ArrayList();
		al.Add (pic1);
		al.Add (pic2);
		al.Add(pic3);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.anyKeyDown) {
			sNum++;
			if (sNum >= al.Count)
				Application.LoadLevel("Title page");
			else{
				SpriteRenderer sr = GetComponent<SpriteRenderer>();
				sr.sprite = al[sNum] as Sprite;
			}
		}
	}
}
