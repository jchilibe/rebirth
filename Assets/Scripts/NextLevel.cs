﻿using UnityEngine;
using System.Collections;

public class NextLevel : MonoBehaviour {

	public string nextLevelName;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Player") {
			GameObject.FindWithTag ("MainCamera").GetComponent<CameraControl>().levelComplete = true;
			col.gameObject.GetComponent<ShipMove>().levelComplete = true;
			var tp = col.gameObject.transform.position;
			col.gameObject.transform.position = tp;
			col.gameObject.rigidbody2D.velocity = new Vector2(0,0);
			col.gameObject.rigidbody2D.angularVelocity = 0;
		}
	}
}
