﻿using UnityEngine;
using System.Collections;

public class BombMouse : MonoBehaviour {
	
	public GameObject targetObj;
	public GameObject anim;
	public float range = 10.0f;
	public float power = 200.0f;

	// Use this for initialization
	void Start () {
		anim.SetActive(false);
		
		Vector3 s = new Vector3();
		s.x = range*2.5f;
		s.y = range*2.5f;
		s.z = 1;
		anim.transform.localScale = s;
	}

	void destroyBomb(){
		Destroy(gameObject);
	}

	void explode(){
		print ("Exploded!");
		Destroy(gameObject);
	}
	
	void OnMouseDown(){
		Vector2 dist = (transform.position - targetObj.transform.position);
		if (dist.magnitude < range){
			float multiplier = 1-(dist.magnitude/range);
			multiplier = Mathf.Max (multiplier,0);
			targetObj.rigidbody2D.AddForce(-dist.normalized * multiplier * power);
		}
		/*AnimationClip clip = animation["Blast"].clip;
		AnimationEvent destroy = new AnimationEvent();
		destroy.functionName = "DestroyBomb";
		destroy.time = clip.length;*/
		anim.SetActive(true);
	}
}