﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {
	Vector3 initialPoint;
	public float rSpeed = 50;

	// Use this for initialization
	void Start () {
		initialPoint = transform.position;
		rigidbody2D.fixedAngle = false;
		rigidbody2D.isKinematic = false;
		rigidbody2D.mass = 1000;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		gameObject.transform.position = initialPoint;
		gameObject.rigidbody2D.angularVelocity = rSpeed;
	}
}
