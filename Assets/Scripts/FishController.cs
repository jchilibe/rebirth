﻿using UnityEngine;
using System.Collections.Generic;

public class FishController : MonoBehaviour {

	public GameObject fish;
	public int fishLeft = 5;
	List<GameObject> fishList;

	// Use this for initialization
	void Start () {
		fishList = new List<GameObject>();
		for (int i=0;i<fishLeft;i++)
		{
			GameObject fClone = (GameObject)Instantiate(fish);
			fClone.transform.parent = transform;
			fClone.transform.localPosition = new Vector3(i*3,0,0);
			fishList.Add(fClone);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public int getFish()
	{
		return fishLeft;
	}

	public void loseFish()
	{
		fishLeft--;
		Destroy(fishList[fishLeft]);
		fishList.RemoveAt(fishLeft);
	}
}
