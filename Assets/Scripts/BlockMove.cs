﻿using UnityEngine;
using System.Collections;

public class BlockMove : MonoBehaviour {

	public float moveRange;
	public float moveSpeed;

	float currentPos = 0;
	int moveDir = 1; // 1 or -1 for direction

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Quaternion rt = new Quaternion();
		rt.eulerAngles = new Vector3(0,0,(currentPos/moveRange)*30-15);
		transform.rotation = rt;
		rigidbody2D.velocity = new Vector2(moveSpeed*moveDir,0);
		currentPos += moveSpeed*moveDir;
		if (currentPos > moveRange) moveDir = -1;
		else if (currentPos < 0) moveDir = 1;
	}
}
