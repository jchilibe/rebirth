﻿using UnityEngine;
using System.Collections;

public class EnemyReset : MonoBehaviour {
	Vector3 initPos;
	public GameObject player;

	// Use this for initialization
	void Start () {
		initPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject == player) {
			player.GetComponent<ShipMove>().resetPos();
			gameObject.transform.position = initPos;
			gameObject.rigidbody2D.velocity = new Vector2(0,0);
		}
	}
}
