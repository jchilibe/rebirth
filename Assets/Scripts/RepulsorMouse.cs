﻿using UnityEngine;
using System.Collections;

public class RepulsorMouse : MonoBehaviour {

	public GameObject targetObj;
	//public Camera cam;
	public GameObject anim;
	public float range = 10.0f;
	public float power = 50.0f;
	bool mouseDown=false;
	int bombTimer = -1;
	// Use this for initialization
	void Start () {
		anim.SetActive(false);
		mouseDown=false;

		Vector3 s = new Vector3();
		s.x = range*2.5f;
		s.y = range*2.5f;
		s.z = 1;
		//transform.localScale = new Vector3(range/10,range/10,1);
		anim.transform.localScale = s;
	}
	
	// Update is called once per frame
	void Update () {
		if (bombTimer == 0) explode();
	}

	void FixedUpdate(){
		if (mouseDown || bombTimer>0){
			Vector2 dist = (transform.position - targetObj.transform.position);
			if (dist.magnitude < range){
				float multiplier = 1-(dist.magnitude/range);
				multiplier = Mathf.Max (multiplier,0);
				targetObj.rigidbody2D.AddForce(-dist.normalized * multiplier * power);
			}
			//cam.GetComponent<CameraControl>().screenShake = 1;
			bombTimer--;
		}
	}

	void explode(){
		print ("Exploded!");
		Destroy(gameObject);
	}

	void OnMouseOver(){
		if (Input.GetMouseButtonDown(1)){
			anim.SetActive(true);
			bombTimer = 100;
		}
	}

	void OnMouseDown(){
		mouseDown=true;
		anim.SetActive(true);
	}

	void OnMouseUp(){
		mouseDown = false;
		anim.SetActive(false);
	}
}