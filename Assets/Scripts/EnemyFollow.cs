﻿using UnityEngine;
using System.Collections;

public class EnemyFollow : MonoBehaviour {

	public GameObject player;
	public float moveSpeed = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 dist = (transform.position - player.transform.position);
		rigidbody2D.AddForce(-dist.normalized * moveSpeed);
	}
}
